# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   © 2007 Christoph Berg <myon@debian.org>
#   Copied from rt.py:
#   © 2006 Sanghyeon Seo <sanxiyn@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

import urllib, urlparse, cgi

from BeautifulSoup import BeautifulSoup
from base import RemoteReport
from __init__ import *

# <tr><td nowrap><b>State:</b></td>
# <td><tt>open</tt></td></tr>

def parse_table(soup, key):
    cell = soup.firstText(key).findParent('td')
    return cell.findNextSibling('td').first('tt').string

# Cookie: gnatsweb-global=email&cb%40df7cb.de&database&mutt&Submitter-Id&any&columns&Notify-List%20Category%20Synopsis%20Confidential%20Severity%20Priority%20Responsible%20State%20Keywords%20Date-Required%20Class%20Submitter-Id%20Arrival-Date%20Closed-Date%20Last-Modified%20Originator%20Release; gnatsweb-db-mutt=password&g%60vft&user&guest

class GnatsData(RemoteReport):
    def __init__(self, bts, uri, id):
        self.bts = bts
        self.uri = uri
        soup = BeautifulSoup(wget(uri, "cookies.txt"))

        self.id = id or failwith(uri, "Gnats: no id")
        self.status = parse_table(soup, 'State:') or failwith(uri, "Gnats: no status")
        self.resolution = None

class RemoteGnats(RemoteBts):
    def __init__(self, cnf):
        bugre  = r'^%(uri)s/([0-9]+)$'
        urifmt = '%(uri)s/%(id)s'
        RemoteBts.__init__(self, cnf, bugre, urifmt, GnatsData)

    def isClosing(self, status, resolution):
        return status in ('closed',)

    def isWontfix(self, status, resolution):
        return status in ('suspended',)

RemoteBts.register('gnats', RemoteGnats)
