# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   © 2006 Pierre Habouzit <madcoder@debian.org>
#   © 2008 Jelmer Vernooij <jelmer@samba.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

import sys, os, re, time, traceback

__docformat__ = 'restructuredText'

class ParseExn(Exception):
    def __init__(self, url, reason = None):
        self.url    = url
        self.reason = reason

    def __str__(self):
        return  "Parse error: [%s] %s" % (self.url, self.reason or "")


class DupeExn(Exception):
    def __init__(self, url):
        self.url = url

    def __str__(self):
        return  "Does not deals dupes: [%s]" % (self.url)


class RemoteReport:
    """Describes a bug report that exists in a remote bug tracker."""

    def __init__(self, bts, id, status, resolution):
        self.bts = bts

        self.id         = id
        self.status     = status
        self.resolution = resolution
        self.uri        = self.bts.getUri(self.id)

        self.closed     = self.bts.isClosing(self.status, self.resolution)
        self.wontfix    = self.bts.isWontfix(self.status, self.resolution)

    def __repr__(self):
        return "%s(%s)" % (self.__class__.__name__, self.uri)


class RemoteBts:
    """A remote bug tracker."""

    resources = {}
    _remotes  = {}

    ##########################################################################
    # static methods

    @classmethod
    def setup(cls, res):
        for k, v in res.iteritems():
            typ = v['type']
            if typ not in cls._remotes:
                print >> sys.stderr, "`%s' is an unknown remote BTS type" % (typ)
                sys.exit(1)
            v['bts'] = cls._remotes[typ](v)
            cls.resources[k] = v

    @classmethod
    def register(cls, typ, thecls):
        """Register a new bug tracker class.

        :param typ: Name of the bug tracker type.
        :param thecls: Class object for the remote bug tracker.
        """
        cls._remotes[typ] = thecls

    @classmethod
    def get(cls, name):
        """Obtain a bug tracker class by name.

        :param name: Name of the bug tracker.
        """
        return cls._remotes.get(name)

    @classmethod
    def find(cls, uri):
        """Find a bug tracker class by bug URI.

        :param uri: URI of a bug report
        """
        for k, v in cls.resources.iteritems():
            if 'uri-re' in v:
                if v['uri-re'].match(uri):
                    return v['bts']
            elif uri.startswith(v['uri']):
                return v['bts']
        return None

    ##########################################################################
    # public methods

    def __init__(self, cnf, extractRe = None, uriFmt = None, bugCls = None):
        self._cnf    = cnf
        self._bugre  = extractRe and re.compile(extractRe % cnf)
        self._urifmt = uriFmt
        self._bugcls = bugCls

    def getReport(self, uri):
        """Obtain the bug report at a specified URI.

        :param uri: Bug report URI
        """
        return self._getReportData(uri)

    def getReports(self, uris):
        """Mass-obtain a set of bug reports.

        :param uris: List of bug report URIs.
        """
        for uri in uris:
            yield self.getReport(uri)

    def extractBugid(self, uri):
        """Extra the bug id from a bug report URI.

        :param uri: Bug report URI.
        """
        if 'uri-re' in self._cnf:
            m = self._cnf['uri-re'].match(uri)
            return m and m.group(1)
        else:
            return self._extractBugid(uri)

    def getUri(self, bugId):
        """Find the bug URI for a bug id in this bug tracker.

        :param bugId: Id of bug.
        """
        if 'bugfmt' in self._cnf:
            return self._cnf['bugfmt'] % (bugId)
        return self._getUri(bugId)

    ##########################################################################
    # public virtuals

    def isClosing(self, status, resolution): 
        raise NotImplementedError(self.isClosing)

    def isWontfix(self, status, resolution): 
        raise NotImplementedError(self.isWontFix)

    ##########################################################################
    # protected virtual methods

    def _extractBugid(self, uri):
        assert self._bugre
        m = self._bugre.match(uri)
        return m and m.group(1)

    def _getUri(self, bugId):
        assert self._urifmt
        return self._urifmt % {'uri': self._cnf['uri'], 'id': bugId}

    def _getReportData(self, uri):
        assert self._bugcls
        return self._bugcls(self, uri, self.extractBugid(uri))

    def _bugId(self, data):
        return data.id

import berlios, bugzilla, gnats, launchpad, mantis, rt, savane, sourceforge, trac

