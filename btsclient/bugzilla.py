# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   © 2006 Pierre Habouzit <madcoder@debian.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

import re, sys, urllib, time, traceback, os
from __init__ import *
from base import RemoteReport
from BeautifulSoup import BeautifulSoup

class OldBugzillaData(RemoteReport):
    dupre = re.compile(r'\*\*\* This (?:bug|issue) has been marked as a duplicate of(?: bug)? ([0-9]+) \*\*\*$')

    def __init__(self, bts, uri):
        self.bts = bts
        self.uri = uri
        soup = BeautifulSoup(wget(uri))

        if soup.bugzilla:
            bug         = soup.bugzilla.bug     or failwith(uri, "BugZilla: no bug")
            self.id     = bug.bug_id.string     or failwith(uri, "BugZilla: no bug_id")
            self.status = bug.bug_status.string or failwith(uri, "BugZilla: no bug_status")
        elif soup.issuezilla:
            bug         = soup.issuezilla.issue   or failwith(uri, "IssueZilla: no issue")
            self.id     = bug.issue_id.string     or failwith(uri, "IssueZilla: no issue_id")
            self.status = bug.issue_status.string or failwith(uri, "IssueZilla: no issue_status")
        else:
            failwith(uri, "BugZilla: Invalid XML")

        self.resolution = (bug.resolution and bug.resolution.string) or None
        self.duplicate  = None

        if self.resolution == 'DUPLICATE':
            for t in bug.fetch('thetext')[-1::-1]:
                m   = OldBugzillaData.dupre.search(t.string)
                bug = m and m.group(1)
                if bug:
                    self.duplicate = bug
                    break

            if not self.duplicate:
                failwith(uri, "BugZilla: cannot find duplicate")

class BugzillaData(RemoteReport):
    dupre = re.compile(r'\*\*\* This (?:bug|issue) has been marked as a duplicate of(?: bug)? ([0-9]+) \*\*\*$')

    def __init__(self, bts, uri, bug):

        self.uri    = uri
        self.bts    = bts
        self.id     = bug.bug_id.string     or failwith("?", "BugZilla: no bug_id")
        self.status = bug.bug_status.string or failwith(self.id, "BugZilla: no bug_status")

        self.resolution = (bug.resolution and bug.resolution.string) or None
        self.duplicate  = None

        if self.resolution == 'DUPLICATE':
            if bug.dup_id:
                self.duplicate = bug.dup_id.string
                return

            for t in bug.fetch('thetext')[-1::-1]:
                m   = OldBugzillaData.dupre.search(t.string)
                bug = m and m.group(1)
                if bug:
                    self.duplicate = bug
                    break

            if not self.duplicate:
                failwith(self.id, "BugZilla: cannot find duplicate")

class OldRemoteBugzilla(RemoteBts):
    def __init__(self, cnf):
        bugre  = r"^%(uri)s/show_bug.cgi\?id=([0-9]+)$"
        urifmt = "%(uri)s/show_bug.cgi?id=%(id)s"
        RemoteBts.__init__(self, cnf, bugre, urifmt)

    def __getClosingStatus(self):
        if 'closing' in self._cnf:
            return self._cnf['closing']

        config = urllib.urlopen(self._cnf['uri'] + '/config.cgi')
        for l in config.readlines():
            if l.startswith('var status_closed'):
                s = l[l.find('['):].strip('[] ,;\r\t\n')
                self._cnf['closing'] = [x.strip("' ") for x in s.split(',')]
                config.close()
                return self._cnf['closing']


    def isClosing(self, status, resolution):
        return resolution != 'WONTFIX' and status in self.__getClosingStatus()

    def isWontfix(self, status, resolution):
        return resolution == 'WONTFIX'

    def _getReportData(self, uri):
        id = self.extractBugid(uri)
        if not id: return None

        uri  = "%s/xml.cgi?id=%s" % (self._cnf['uri'], id)
        data = OldBugzillaData(uri)
        while data.resolution == 'DUPLICATE':
            uri  = "%s/xml.cgi?id=%s" % (self._cnf['uri'], data.duplicate)
            data = OldBugzillaData(uri)

        return data


class RemoteBugzilla(OldRemoteBugzilla):
    def __init__(self, cnf):
        OldRemoteBugzilla.__init__(self, cnf)

    def getReports(self, uris):
        fields = ["bug_id", "bug_status", "resolution", "dup_id", "long_desc"]
        qs  = "ctype=xml&field=" + "&field=".join(fields)
        for uri in uris:
            id = self.extractBugid(uri)
            if id is None:
                failwith(uri, "BugZilla: cannot extract id from fwd: %s" % uri)
            qs += '&id=' + id
        massuri = "%s/show_bug.cgi?%s" % (self._cnf['uri'], qs)

        assoc = {}
        for v, k in subq: assoc[k] = v

        bugs = BeautifulSoup(wget(massuri))
        if not bugs:
            failwith("Bugzilla: invalid XML: %s" % massuri)

        for uri, bug in zip(uris, bugs.bugzilla.fetch('bug')):
            yield BugzillaData(self, uri, bug)

RemoteBts.register('old-bugzilla', OldRemoteBugzilla)
RemoteBts.register('bugzilla', RemoteBugzilla)
