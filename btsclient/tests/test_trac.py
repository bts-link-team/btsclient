# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   © 2008 Jelmer Vernooij <jelmer@samba.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

from cStringIO import StringIO
from btsclient.trac import TracTicket
from btsclient import *
from unittest import TestCase


class TestTracTicket(TestCase):
    def parse(self, text):
        return TracTicket(text)

    def test_tabformat(self):
        ticket = self.parse("""id\tsummary\treporter\towner\tdescription\ttype\tstatus\tpriority\tmilestone\tcomponent\tversion\tresolution\tkeywords\tcc
42\tWe should not foo\tanonymous\tsomebody\tThis is a long description\\r\\nAnd it includes whitespace stuff.\tdefect\tclosed\tnormal\t3.0.2\tcore\t3.0\tfixed\t\t""")
        self.assertEquals(ticket.id, 42)
        self.assertEquals(ticket.summary, "We should not foo")
        self.assertEquals(ticket.milestone, "3.0.2")
        self.assertEquals(ticket.version, "3.0")
        self.assertEquals(ticket.reporter, "anonymous")
        self.assertEquals(ticket.owner, "somebody")
        self.assertEquals(ticket.description, "This is a long description\r\n" + 
                                              "And it includes whitespace stuff.")
        self.assertEquals(ticket.status, "closed")
        self.assertEquals(ticket.priority, "normal")

    def test_fromhtml(self):
        ticket = self.parse("""
        <html><body>
        <span class="status"><strong>(closed fixed)<strong></span>
        </body></html>
""")
        self.assertEquals("closed", ticket.status)
        self.assertEquals("fixed", ticket.resolution)


    def test_fromhtml_noresolution(self):
        ticket = self.parse("""
        <html><body>
        <span class="status"><strong>open<strong></span>
        </body></html>
""")
        self.assertEquals("open", ticket.status)
        self.assertEquals(None, ticket.resolution)

