#!/usr/bin/python

from distutils.core import setup

setup(name='btsclient',
      version="0.5",
      description='Generic bug tracker client',
      keywords='bts remotebts',
      license='BSD',
      url="http://alioth.debian.org/projects/bts-link/",
      maintainer="bts-link Developers",
      maintainer_email="bts-link-devel@lists.alioth.debian.org",
      packages=['btsclient'],
      scripts=['btsinfo'],
      )
